Imagine you could've a wonderful guided tour of best places in a city listed in foursquare, and when we say guided, we really mean it. In simple terms, Perfect Day lets you get inspired to follow interesting and featured stories and also inspire others by sharing your own favorite hangouts in a city.

Browse from many lists → Follow the one you like → Get guided tour & tips all the way!
Imagine you could browse through various wonderful lists created by people, you could browse through them, look out for categories you like and simply start following. Now simply sit back and get ready to be part of a fun-filled journey hand-picked and curated by people. The connected app will keep giving you updates about the next spot in the list once you check-in at a place, additionally it will guide you on how to reach the next place, how far it is and so on. The entire experience will make travelling much more fun and compelling. 

Login with foursquare → Search & Add to list → Publish & Inspire
Many of us have visited lots of places in a city and have fond memories about nice places we went to eat with friends or had nice drinks and so on. Imagine if you could easily plan a day with all these places on foursquare and share it to the world. 
