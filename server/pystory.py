#!/usr/bin/python
import pymongo
class pystory:
	
	db = None
	def __init__(self):
		from pymongo import Connection
		connection = Connection()
		self.db = connection['story-db']
		pass

	def saveuser(self, User):
		return self.db.user.insert(User)

	def checkuser(self, user_id):
		return self.db.user.find_one({"id":user_id}, {"_id":0})

	def savecheckin(self, Checkin):
		return self.db.checkin.insert(Checkin)