#!/usr/bin/python
import MySQLdb
'''
DB Methods
@author: Sandeep
@date: Aug 20, 2012
'''
	
class dbstory:
	
	def __init__(self):
		self.db = MySQLdb.connect("localhost","storydb","11862!!!","storydb" )
		self.cursor = self.db.cursor()
		pass
	
	def reInit(self):
		if self.db.open:
			pass
		else:
			self.db = MySQLdb.connect("localhost","storydb","11862!!!","storydb" )
			self.cursor = self.db.cursor()
		
	def dbQuery(self, queryString):
		self.cursor.execute("SET NAMES 'utf8'")
		self.cursor.execute(queryString)
		query_result = [ dict(line) for line in [zip([ column[0] for column in self.cursor.description], row) for row in self.cursor.fetchall()] ]
		#self.db.close()
		return query_result

	def dbQueryMany(self, queryString):
		self.cursor.execute("SET NAMES 'utf8'")
		result = [{}]
		for query in queryString:
			self.cursor.execute(queryString[query])
			query_result = [ dict(line) for line in [zip([ column[0] for column in self.cursor.description], row) for row in self.cursor.fetchall()] ]
			if query_result:
				result[0].update(query_result[0])
		#self.db.close()
		return result[0]

	def dbQueryManyList(self, queryString):
		valueSet = {}
		for query in queryString:
			self.cursor.execute(queryString[query])
			result = []
			for row in self.cursor.fetchall():
				for column in row:
					result.append(column)
			valueSet[query] = result
		#self.db.close()
		return valueSet

	def dbInsert(self, insertString):
		self.cursor.execute("SET NAMES 'utf8'")
		status = {}
		try:
			self.cursor.execute(insertString)
			self.db.commit()
			#self.db.close()
			status['flag'] = 1
			return status
		except:
			self.db.rollback()
			#self.db.close()
			status['flag'] = 0
			return status

	def dbInsertWithId(self, insertString):
		insertString = str(insertString.encode('utf-8'))
		status = {}
		try:
			self.cursor.execute(insertString)
			inserted_id = self.db.insert_id()
			self.db.commit()
			#self.db.close()
			status['flag'] = 1
			status['id'] = inserted_id
			return status
		except:
			self.db.rollback()
			#self.db.close()
			status = {}
			status['flag'] = 0
			return status
		
	def dbUpdate(self, queryString):
		try:
			self.cursor.execute("SET NAMES 'utf8'")
			self.cursor.execute(queryString)
			#self.db.close()
			data = {}
			data['flag'] = 1
			return data
		except:
			data['flag'] = 0
			self.db.rollback()
			#self.db.close()
			return data

	def dbUpdateMany(self, queryString):
		data = {}
		try:
			for query in queryString:
				self.cursor.execute("SET NAMES 'utf8'")
				self.cursor.execute(queryString[query])
			self.db.commit()
			#self.db.close()
			data['flag'] = 1
			return data
		except:
			self.db.rollback()
			#self.db.close()
			data['flag'] = 0
			return data

	def dbRowExists(self, queryString):
		try:
			self.cursor.execute(queryString)
			row = cursor.fetchone()
			if row == None:
				return False 
			return True
		except:
			return False