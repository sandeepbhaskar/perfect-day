import constants, dbstory
from bson import json_util
import urllib, json, StringIO, pycurl
class Storify:

	def __init__(self):
		pass
	#Product UI Handlers
	def get_user_access_token(self, user_id):
		import listifydb
		db = listifydb.listifydb()
		user_det = db.dbQuery("SELECT access_token "+"FROM User WHERE id='"+user_id+"'")
		if user_det != []:
			return user_det[0]['access_token']
		return []

	def createstory(self, data):
		'''
		data:{
			"name":"Hog @Indirangar",
			"description":"The best places to satisfy your tummy in Indirangar",
			"user_id":"<user_id>",
			"venues": [
				{
					"id":"4c202788eac020a151414cc2",
					"tip":"Do try coin idly"
				},
				{
					"id":"4e89e7c27ee6a22446679382",
					"tip":"Chicken dosa is must-try"
				},
				{
					"id":"4ffabb8ee4b054fbabe99c71",
					"tip":"Pamper yourself with some Natural Ice Cream"
				}
			]
		}
		'''
		#return data

		import listifydb
		db = listifydb.listifydb()
		db_response = db.dbInsertWithId("INSERT INTO Story(user_id, name, description)"+" VALUES('"+str(data.get('user_id'))+"','"+str(data.get('name'))+"','"+str(data.get('description'))+"')")
		if db_response['flag'] == 1:
			seq_no = 1
			story_id = db_response['id']
			for venue in data.get('venues'):
				db.dbInsert("INSERT INTO Story_Det(story_id,seq_no,venue_id,venue_name, tip)"+" VALUES("+str(story_id)+","+str(seq_no)+",'"+str(venue['id'])+"','"+venue['name']+"','"+str(venue['tip'])+"')")
				seq_no = seq_no + 1
			return {"flag":0, "msg":"Done!"}
		else:
			return {"flag":-1, "msg":"Oops!"}

	def getstories(self,data):
		import listifydb
		db = listifydb.listifydb()
		all_stories = []
		stories = db.dbQuery("SELECT story_id,A.name AS story_name,description,featured,user_id,B.name,photo FROM Story A,User B WHERE A.user_id=B.id ORDER BY featured DESC, story_id DESC"  )
		for story in stories:
			follow_yn = self.doesuserfollow(data.get('user_id'),story['story_id'],db)
			if follow_yn == 0:
				story['following'] = "n"
			else:
				story['following'] = "y"
			all_stories.append(story)
		return all_stories

	def getstorydetails(self, data):
		import listifydb
		db = listifydb.listifydb()
		db_response = db.dbQuery("SELECT seq_no, venue_id, venue_name, tip "+" FROM Story_Det WHERE story_id='"+str(data.get('story_id'))+"'")
		return db_response

	def followstory(self, data):
		import listifydb
		db = listifydb.listifydb()
		try:
			db.dbInsert("INSERT INTO UserStory(user_id, story_id)"+" VALUES('"+str(data.get('user_id'))+"','"+str(data.get('story_id'))+"')")
			return {"flag":0, "msg":"Done!"}
		except:
			return {"flag":-1, "msg":"Oops!"}

	def citysearch(self, data):
		suggestions = []
		url = constants.GOOGLE_API_URL+"?input="+str(data.get('q'))+"&types=geocode&sensor=true&key="+constants.GOOGLE_API_KEY
		results = json.loads(urllib.urlopen(url).read())
		if results['status'] == "OK":
			for city in results['predictions']:
				suggest = {
					'id':city['id'],
					'name':city['description']
				}
				suggestions.append(suggest)
		return suggestions

	def venuesearch(self, data):
		suggestions = []
		url = constants.FOURSQUARE_API_URL+'venues/search?query='+data.get('q')+'&near='+data.get('near')+'&oauth_token='+constants.FOURSQUARE_SAMPLE_ACCESS_TOKEN+'&v=20121222'
		results = json.loads(urllib.urlopen(url).read())
		venues = results['response']['venues']
		for venue in venues:
			location = venue['location']
			suggest = {
				'id': venue['id'],
				'name': venue['name']
			}
			if 'address' in location.keys():
				try:
					suggest['loc'] = location.get('address') + ',' + location.get('crossStreet')
				except:
					suggest['loc'] = location.get('address')
			suggestions.append(suggest)
		return suggestions

	#Push and Checkin FSQ Stuff
	def add_push(self, checkin, secret, user):
		try:
			flag = False
			checkin_d = json.loads(checkin)
			user_d = json.loads(user)
			import listifydb
			db = listifydb.listifydb()
			db_response = db.dbInsert("INSERT INTO Checkin(user_id,checkin_id,venue_id,venue_name)"+" VALUES('"+str(user_d['id'])+"','"+str(checkin_d['id'])+"','"+str(checkin_d['venue']['id'])+"','"+str(checkin_d['venue']['name'])+"')")
			
			nxtstories = self.getnextstory(checkin_d['venue']['id'],user_d['id'])
			print nxtstories
			if nxtstories != []:
				if nxtstories[0]['seq_no'] == 2:
					text = constants.STORY_MSG_2
				elif nxtstories[0]['seq_no'] == 3:
					text = constants.STORY_MSG_3
					flag = True
				else:
					text = constants.STORY_MSG_1

				text = text+"\n\n"+str(nxtstories[0]['name'])
				'''if nxtstories[0]['description'] != "":
					text = text+". Description: "+str(nxtstories[0]['description'])'''
				if nxtstories[0]['seq_no'] == 2:
					text = text +"\n\nNext up is "+self.get_venue_name(nxtstories[0]['venue_id'], db)+" (just 0.9 km(s) away)"
				else:
					text = text +"\n\nNext up is "+self.get_venue_name(nxtstories[0]['venue_id'], db)+" (just 0.3 km(s) away)"
				text = text +"\n\nSpecial Tip:"+ str(nxtstories[0]['tip'])
				#text = text +"\n\nClick to check out the venue, read about tips from other foursquare users."
				if flag == True:
					self.checkin_reply(checkin_d['id'],text,user_d['id'],nxtstories[0]['venue_id'], 2)
				else:
					self.checkin_reply(checkin_d['id'],text,user_d['id'],nxtstories[0]['venue_id'], 0)
			else:
				text = "Did you have a good experience here? If so, create your perfect day! "
				self.checkin_reply(checkin_d['id'],text,user_d['id'],checkin_d['venue']['id'], 1)
			return True
		except Exception, e:
			print "Exception %s"% e

	def get_venue_name(self, venue_id, db):
		venue_det = db.dbQuery("SELECT venue_name FROM Checkin "+"WHERE venue_id='"+str(venue_id)+"'")
		try:
			return venue_det[0]['venue_name']
		except:
			return ''

	def getnextstory(self, venue_id, user_id):
		import listifydb
		db = listifydb.listifydb()
		nxtstories = []
		#should check if this user follows this list
		current_stories = db.dbQuery("SELECT story_id, seq_no, tip FROM Story_Det "+"WHERE venue_id='"+str(venue_id)+"'")
		for story in current_stories:
			if self.doesuserfollow(user_id, story['story_id'], db) == 0:
				print 'User: '+str(user_id)+' doesnt follow '+str(story['story_id'])
				pass
			else:
				nxtseqno = story['seq_no']+1
				nxtstory = db.dbQuery("SELECT A.name, A.description, B.tip, B.venue_id,B.seq_no "+" FROM Story A, Story_Det B WHERE B.story_id = '"+str(story['story_id'])+"' AND A.story_id = B.story_id AND B.seq_no = "+str(nxtseqno))
				for nxtstory_detail in nxtstory:
					nxtstories.append(nxtstory_detail)
		return nxtstories

	def doesuserfollow(self, user_id, story_id, db):
		follow = db.dbQuery("SELECT user_id FROM UserStory"+" WHERE user_id='"+str(user_id)+"' AND story_id='"+str(story_id)+"'")
		if follow == []:
			return 0
		return 1


	def checkin_reply(self, checkin_id, text, user_id, venue_id, reply_type):
		try:
			#import time
			#time.sleep(2)
			url = constants.FOURSQUARE_API_URL+'checkins/'+checkin_id+'/reply?oauth_token='+self.get_user_access_token(user_id)
			if reply_type == 0:
				params = {
					'CHECKIN_ID': str(checkin_id),
					'text': str(text),
					'url':constants.FOURSQUARE_VENUE_URL+str(venue_id)
				}
			elif reply_type == 2:
				params = {
					'CHECKIN_ID': str(checkin_id),
					'text': str(text),
					'url':constants.FOURSQUARE_USER_URL
				}
			else:
				params = {
					'CHECKIN_ID': str(checkin_id),
					'text': str(text),
					'url':constants.STORIFY_URL					
				}
			post_params = urllib.urlencode(params)
			b = StringIO.StringIO()
			ch = pycurl.Curl()
			ch.setopt(pycurl.URL, str(url))
			ch.setopt(pycurl.POST, 1)
			ch.setopt(pycurl.POSTFIELDS, post_params)
			ch.setopt(pycurl.WRITEFUNCTION, b.write)
			ch.perform()
			ch.close()
			print 'Reply sent to '+str(user_id)+'; params'+str(params)
			print 'response'
			print b.getvalue()
			return True
		except Exception, e:
			print "Exception in checkin_reply %s" % e

	#Callback Methods
	def callback(self, data):
		code = data.get('code')
		import listifydb
		db = listifydb.listifydb()
		User = self.fs_get_user_details(self.fs_get_access_token(code), 'self')
		db.dbInsert("INSERT INTO User(id,name,bio,city,email,photo,access_token)"+" VALUES('"+str(User['id'])+"','"+str(User['name'])+"','"+str(User['bio'])+"','"+str(User['city'])+"','"+str(User['email'])+"','"+str(User['photo'])+"','"+str(User['access_token'])+"')")
		return User
		
	def fs_get_access_token(self, code):
		url = constants.FOURSQUARE_OAUTH_URL+'?client_id='+constants.FOURSQUARE_CLIENT_ID+'&client_secret='+constants.FOURSQUARE_CLIENT_SECRET+'&grant_type=authorization_code'+'&redirect_uri='+constants.FOURSQUARE_CALLBACK_URL+'&code='+code
		results = json.loads(urllib.urlopen(url).read())
		return results

	def fs_get_user_details(self, access_token, user_id):
		url = constants.FOURSQUARE_API_URL+'users/'+user_id+'?oauth_token='+access_token['access_token']
		results = json.loads(urllib.urlopen(url).read())
		user_details = results['response']['user']
		User = {
			'id':user_details['id'],
			'name':user_details['firstName']+' '+user_details['lastName'],
			'access_token': access_token['access_token'],
			'email':user_details['contact']['email'],
			'city':user_details['homeCity'],
			'bio':user_details['bio'],
			'photo':user_details['photo'],
		}
		return User

	def fs_get_venue_photos(self, access_token, venue_id):
		url = constants.FOURSQUARE_API_URL+'venues/'+str(venue_id)+'/photos?group=venue'
		results = json.loads(urllib.urlopen(url).read())
		photos = results['response']['photos']['items']
		if photos == []:
			return ''
		else:
			return photos[0]['prefix']+str('30X30')+photos[0]['suffix']

	def hello(self):
		return 'Hello Storify'
