(function(){
    var userId=3788734;
    var currentPlace;
    var venuePanel;
    var currentVenueTxtBox;
    Ext.define('ListifyMe.controller.Home',{
    extend: 'Ext.app.Controller',

    config: {
        control: {
                'home': {
                    back: function(){
                        this.getHomeContainer().down('#createBtn').hide();
                    }
                },
                'home #placeSearchField': {
                    action:function(searchFieldRef){
                        this.showPlaceList(searchFieldRef.getValue());
                    }
                },
                'home #createBtn': {
                    tap: function(){
                        this.createVenueList();
                    }

                },
                'placelist' : {
                    itemtap: function(list,index,target,record,e,eOpts){
                        this.goTocreateVenue(list,index,target,record,e,eOpts);                        
                        this.getHomeContainer().down('#createBtn').show();
                    }
                },
                'venueform #venueFormSearchField': {
                    action: function(searchFieldRef){
                        venuePanel = Ext.create('Ext.Panel', {
                            layout: 'fit',
                            items: this.getVenueListTpl(searchFieldRef),
                            modal: true,
                            hideOnMaskTap: true,
                            width: 300,
                            height: '70%',
                            left: 0,
                            top: 0
                        });
                        Ext.Viewport.add(venuePanel);
                    }
                },
                'venuelist' : {
                    itemtap: function(list,index,target,record,e,eOpts){
                        currentVenueTxtBox.setValue(record.get('name'));
                        currentVenueTxtBox.up().down('#venueId').setValue(record.get('id'));
                        venuePanel.hide();
                    }
                },
                'timeline': {
                    itemtap: function(list,index,target,record,e,eOpts){
                        list.up().up().push({
                            title: 'Second',
                            html: 'Second view!'
                        });
                    }
                },
                scope: this
        },

        refs: {
            placeList: 'placelist',
            homeContainer: 'home',
            venueForm: 'venueform',
            venueList: 'venuelist',
            venueFormSearchField: 'venueform #venueFormSearchField',
            timeLine: 'timeline'
        },

        init: function(){

        }
    },


    showPlaceList: function(searchParam){
        this.getPlaceList().mask({
            xtype: 'loadmask',
            message: 'Getting locations'
        });
        this.getPlaceList().getStore().load({params:{q:searchParam,api:'citysearch'}});
        this.getPlaceList().show();
    },

    getVenueListTpl: function(searchRef){
        var list = Ext.create('ListifyMe.view.VenueList');
        list.getStore().load({params:{q:searchRef.getValue(),api:'venuesearch',near:currentPlace}});
        list.mask({
            xtype: 'loadmask',
            message: 'Refreshing places'
        });
        currentVenueTxtBox = searchRef;
        return list;
    },

    goTocreateVenue: function(list,index,target,record,e,eOpts){
        currentPlace = record.get('name');
        list.up().up().push({
            xtype: 'venueform'
        });
    },
    createVenueList:function(){
        var allForms = this.getVenueForm().query('fieldset');
        var venueListJsonData = {};
        venueListJsonData.venues = [];
        for(var i=0; i<allForms.length; i++){
            venueListJsonData.venues.push({
                "id": allForms[i].down('#venueId').getValue(),
                "tip": allForms[i].down('#venueComments').getValue(),
                "name": allForms[i].down('#venueFormSearchField').getValue()
            });
        }
        venueListJsonData.name = this.getVenueForm().down("#listName").getValue();
        var finalJsonObj = {
            "name": venueListJsonData.name,
            "venues": venueListJsonData.venues,
            "user_id": userId
        };
        var containerWindow = this.getHomeContainer();
        console.log(JSON.stringify(finalJsonObj));
        Ext.data.JsonP.request({
            url: 'http://sandeepbhaskar.com/projects/storify/api.php?api=createstory',
            params: {
                data: JSON.stringify(finalJsonObj)
            },
            success: function(result, request) {
                console.log(result)
                Ext.Msg.alert('', 'Your list is created now. Create more lists or go timeline', function(){
                    containerWindow.up().setActiveItem(2);
                });
            },
        });
   
    }
});
})();