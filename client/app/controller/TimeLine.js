(function(){
    //
    var storyId;
    var userId=3788734;
    Ext.define('ListifyMe.controller.TimeLine',{
    extend: 'Ext.app.Controller',

    config: {
        control: {
            'timeline': {
                back: function(){
                    this.getTimeLine().down("#followBtn").hide();
                    this.getTimeLine().down("#followBtn").setText("Follow");
                    this.getTimeLineList().getStore().load({params:{user_id: userId}});
                },
                activate: function(){
                    //user_id: userId
                    this.getTimeLineList().getStore().load({params:{user_id: userId}});
                }
            },
            'timeline timelinelist': {
                itemtap: function(list,index,target,record,e,eOpts){
                    this.createDetailsView(list,record);
                    storyId = record.get('story_id');
                    this.getTimeLine().down("#followBtn").show();
                    //debugger;
                    if (record.get('following') === 'y') {
                        this.getTimeLine().down("#followBtn").setText("Following")
                    };
                }
            },
            'timeline #followBtn': {
                tap: function(btnRef){
                    Ext.data.JsonP.request({
                        url: 'http://sandeepbhaskar.com/projects/storify/api.php?&api=followstory',
                        params: {
                            user_id: userId,
                            story_id: storyId
                        },
                        success: function(result, request) {
                            btnRef.setText('Following');
                        }
                    });
                }
            },
            scope: this
        },

        refs: {
            timeLine: 'timeline',
            timeLineList: 'timelinelist',
            followBtn: 'timeline #followBtn'
        },

        init: function(){

        }
    },
    createDetailsView: function(list,record){

        Ext.data.JsonP.request({
            url: 'http://sandeepbhaskar.com/projects/storify/api.php?callback=fn&api=getstorydetails',
            params: {
                q: record.get('story_id')
            },
            success: function(result, request) {
                var timelineTpl = "";
                for(var i=0;i<result.length; i++){
                    timelineTpl+= '<div style="padding:5px">' + result[i].venue_name + '<div style="color:grey;font-size:14px">'+ "tip: " + result[i].tip+'</div>' + '</div>' + '<hr>' ;
                }
                list.up().up().push({
                    title: record.get('story_name'),
                    html: timelineTpl
                });
            }
        });
    }
});
})();