Ext.define('ListifyMe.model.Venues',{
	extend: 'Ext.data.Model',

	config: {
		fields: ['name']
	}
});