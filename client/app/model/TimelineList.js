Ext.define('ListifyMe.model.TimelineList',{
	extend: 'Ext.data.Model',
	config: {
		fields: ['name','story_name','story_id','user_id','photo','following']
	}
});