Ext.define('ListifyMe.model.VenueList',{
	extend: 'Ext.data.Model',

	config: {
		fields: ['name','id']
	}
});