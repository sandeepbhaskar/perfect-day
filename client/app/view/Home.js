Ext.define('ListifyMe.view.Home', {
	extend: 'Ext.NavigationView',
	xtype: 'home',
	config:{

        	title: 'Create',
                iconCls: 'action',
                fullScreen: true,
                navigationBar: {
                        items: {
                                xtype:'button',
                                text: 'Create',
                                align: 'right',                                
                                itemId: 'createBtn',
                                ui: 'forward',
                                hidden: true
                        }
                },
                items: [
                	{
        		title: 'Add Venue',
        		xtype: 'panel',  
        		layout: 'fit',
        		items:[
        			{
	                    xtype: 'toolbar',
	                    docked: 'top',	                    

	                    items:[
	                        { xtype: 'spacer' },
	                        {
	                            xtype: 'searchfield',
                                    itemId: 'placeSearchField',
	                            placeHolder: 'Search for a place'
	                        },
	                        { xtype: 'spacer' }
	                        ]
                                },
                        	{
                        		xtype: 'placelist',
                        		hidden: true          		
                        	}
        		]
                	}
                ]
	}
});


