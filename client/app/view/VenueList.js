Ext.define('ListifyMe.view.VenueList',{
	extend: 'Ext.List',
	xtype: 'venuelist',
	config: {
        itemTpl: '{name}',
		store: 'VenueList'
	}
});