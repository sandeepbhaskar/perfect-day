Ext.define('ListifyMe.view.TimelineList',{
	extend: 'Ext.List',
	xtype: 'timelinelist',
	config: {
        itemTpl: '<img src="{photo}" height="50px" width="50px"/><span style="margin-left:10px;margin-top:0px">{story_name}</span><div style="color:grey;font-size:10px">by {name}</div>',
		store: 'TimelineList'
	}
});