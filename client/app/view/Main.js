Ext.define('ListifyMe.view.Main', {
    extend: 'Ext.TabPanel',
    
    config:{
        fullscreen: true,
        tabBarPosition: 'bottom',

        defaults: {
            styleHtmlContent: true,
            scrollable: true
        },

        items: [
            {
                title: 'Home',
                iconCls: 'home',
                html: "<div><h2> A wonderful guided tour of best places in a city listed in foursquare, and when we say guided, we really mean it</h2><div  class='stepsCls'><div>1. Create your own list</div><div>2. Follow your own or list created by others</div><div>3. Now when you checkin to any destination we guide you </div></div>" 
            },
            {
                xtype: 'home',
            },
            {
                xtype: 'timeline',
            }
        ]    
    }
    
});