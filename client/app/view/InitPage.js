Ext.define('ListifyMe.view.InitPage', {
    extend: 'Ext.form.FieldSet',
    xtype: 'initpage',
    config:{
        title: 'You can create a list in 3 simple steps'
    }
    
});