Ext.define('ListifyMe.view.VenueForm',{
	extend:'Ext.form.Panel',
	xtype: 'venueform',
	config:{
		items: [
		{
                    xtype: 'textfield',
                    itemId: 'listName',
                    name : 'listname',
                    label: 'ListName'
        }, 
        {
            xtype: 'fieldset',
            title: 'Add 3 awesome place of your choice',
            items: [           	               
                {
                    xtype: 'searchfield',
                    itemId: 'venueFormSearchField',
                    name : 'venue',
                    placeHolder: 'Search for venue you like to add',
                    label: 'Venue'
                },
                {
                	xtype: 'hiddenfield',
                    itemId: 'venueId'
                }, 
                {
                    xtype: 'textareafield',
                    itemId: 'venueComments',
                    name : 'comments',
                    label: 'Comments'
                }                
            ]
        },
        {
            xtype: 'fieldset',
            instructions: 'Click on Create to complete list creation',
            items: [           	               
                {
                    xtype: 'searchfield',
                    itemId: 'venueFormSearchField',
                    name : 'venue',
                    placeHolder: 'Search for venue you like to add',
                    label: 'Venue'
                },
                {
                	xtype: 'hiddenfield',
                    itemId: 'venueId'
                },
                {
                    xtype: 'textareafield',
                    itemId: 'venueComments',
                    name : 'comments',
                    label: 'Comments'
                }
            ]
        },
        {
            xtype: 'fieldset',
            instructions: 'Click on Create to complete list creation',
            items: [                           
                {
                    xtype: 'searchfield',
                    itemId: 'venueFormSearchField',
                    name : 'venue',
                    placeHolder: 'Search for venue you like to add',
                    label: 'Venue'
                },
                {
                    xtype: 'hiddenfield',
                    itemId: 'venueId'
                },
                {
                    xtype: 'textareafield',
                    itemId: 'venueComments',
                    name : 'comments',
                    label: 'Comments'
                }
            ]
        }
    ]
	}
});