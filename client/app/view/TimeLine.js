Ext.define('ListifyMe.view.TimeLine', {
	extend: 'Ext.NavigationView',
	xtype: 'timeline',
	config:{

        	title: 'TimeLine',
            iconCls: 'star',
            fullScreen: true,
            navigationBar: {
	            items: {
	                    xtype:'button',
	                    text: 'Follow',
	                    align: 'right',                                
	                    itemId: 'followBtn',
	                    ui: 'forward',
	                    hidden: true
	            }
	        },
            items: [
            	{
					xtype: 'panel',  
					layout: 'fit',
	        		items:[
	        			{
	        				xtype: 'timelinelist',
	                	}
	                ]
				}
			]
	}
});


