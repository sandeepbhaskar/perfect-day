Ext.define('ListifyMe.view.PlaceList',{
	extend: 'Ext.List',
	xtype: 'placelist',
	config: {
        itemTpl: '{name}',
		store: 'Venues'
	}
});