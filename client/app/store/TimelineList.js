Ext.define('ListifyMe.store.TimelineList',{
	extend: 'Ext.data.Store',

	config: {
		model: 'ListifyMe.model.TimelineList',
		proxy: {
			type: 'jsonp',
			url: 'http://sandeepbhaskar.com/projects/storify/api.php?api=getstories',
			rootProperty: 'response'
			
		},
		autoLoad: false
	}
});