Ext.define('ListifyMe.store.Venues',{
	extend: 'Ext.data.Store',

	config: {
		model: 'ListifyMe.model.Venues',
		proxy: {
			type: 'jsonp',
			url: 'http://sandeepbhaskar.com/projects/storify/api.php',
			rootProperty: 'response'
			
		},
		autoLoad: false
	}
});