Ext.define('ListifyMe.store.VenueList',{
	extend: 'Ext.data.Store',

	config: {
		model: 'ListifyMe.model.VenueList',
		proxy: {
			type: 'jsonp',
			url: 'http://sandeepbhaskar.com/projects/storify/api.php',
			rootProperty: 'response'
			
		},
		autoLoad: false
	}
});